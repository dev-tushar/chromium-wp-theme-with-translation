<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://themes.zone/
 * @since      1.0.0
 *
 * @package    Tz_Feature_Pack
 * @subpackage Tz_Feature_Pack/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * @package    Tz_Feature_Pack
 * @subpackage Tz_Feature_Pack/admin
 * @author     Themes Zone <themes.zonehelp@gmail.com>
 */
class Tz_Feature_Pack_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {
		$this->plugin_name = $plugin_name;
		$this->version = $version;
	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {
		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/tz-feature-pack-admin.css', array(), $this->version, 'all' );
	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {
		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/tz-feature-pack-admin.js', array( 'jquery' ), $this->version, false );
	}

    public function add_wc_settings_tab( $settings_tabs ){
        $settings_tabs['settings_tab_chromium'] = esc_html__( 'Chromium Custom Taxonomies', 'tz-feature-pack' );
        return $settings_tabs;
    }

    public function settings_tab() {
        woocommerce_admin_fields( $this->get_settings() );
    }

    private function get_settings(){
        $settings = array(
            'section_title' => array(
                'name'     => esc_html__( 'Turn on custom taxonomies', 'tz-feature-pack' ),
                'type'     => 'title',
                'desc'     => '',
                'id'       => 'tz_f_p_custom_taxonomies'
            ),

            array(
                'title'    => esc_html__( 'Enable Brand/Year/Model Taxonomy', 'tz-feature-pack' ),
                'desc'     => esc_html__( 'Enable Brand/Year/Model Taxonomy for your products.', 'tz-feature-pack' ),
                'id'       => 'tz_brand_year_model_taxonomy',
                'default'  => 'no',
                'type'     => 'checkbox',
                'desc_tip' => esc_html__( 'Checking this field will turn on brand/year/model taxonomy', 'tz-feature-pack' ),
            ),

            array(
                'title'    => esc_html__( 'Enable Tire Size Taxonomy', 'tz-feature-pack' ),
                'desc'     => esc_html__( 'Enable Tire Size Taxonomy for your products.', 'tz-feature-pack' ),
                'id'       => 'tz_tire_sizes_taxonomy',
                'default'  => 'no',
                'type'     => 'checkbox',
                'desc_tip' => esc_html__( 'Checking this field will turn on tire size taxonomy', 'tz-feature-pack' ),
            ),

            'section_end' => array(
                'type' => 'sectionend',
                'id' => 'tz_f_p_custom_taxonomies_end'
            )
        );
        return apply_filters( 'tz_f_p_custom_taxonomies_settings', $settings );
    }

    function update_settings() {
        woocommerce_update_options( $this->get_settings() );
    }

}
